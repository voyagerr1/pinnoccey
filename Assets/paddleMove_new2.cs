﻿using UnityEngine;
using System.Collections;

public class paddleMove_new2 : MonoBehaviour {



	public Rigidbody rb;
	public float speedF;
	public float speedR;
	public string inputKeyName;
	public AudioSource ad;
	public AudioClip up;
	public AudioClip down;
	
	// Use this for initialization
	void Start () {
		rb = GetComponent<Rigidbody> ();
		ad = GetComponent<AudioSource> ();
	}
	
	// Update is called once per frame
	void Update () 
	{
		if (Input.GetButton (inputKeyName)) {
			rb.AddForce (transform.forward * speedF);

		} else  {
			rb.AddForce (-transform.forward * speedR);
		}
		if (Input.GetButtonDown (inputKeyName)) {
			ad.PlayOneShot(up, 0.7f);
			
		}else if (Input.GetButtonUp (inputKeyName)){
			ad.PlayOneShot(down,0.7f);
		}


		
	}



}
