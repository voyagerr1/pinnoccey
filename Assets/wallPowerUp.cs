﻿using UnityEngine;
using System.Collections;

public class wallPowerUp : MonoBehaviour {

	public BoxCollider bc;
	public MeshRenderer mr;
	public bool canUse = true;
	public string inputKey;

	// Use this for initialization
	void Start () {
		bc = GetComponent<BoxCollider> ();
		mr = GetComponent<MeshRenderer> ();
		canUse = true;
	}
	
	// Update is called once per frame
	void Update () {
	
		if (Input.GetButtonDown (inputKey)&& canUse == true) {

			bc.isTrigger = false;
			
			mr.enabled = true;
			Debug.Log ("enabled wall");
			StartCoroutine(waitSome());

		}
	}

		IEnumerator waitSome(){
		
		yield return new WaitForSeconds (3);
		canUse = false;
		bc.isTrigger = true;
		mr.enabled = false;
		Debug.Log ("disabled wall");
		yield return new WaitForSeconds (20);
		canUse = true;
	}
}
