﻿using UnityEngine;
using System.Collections;

public class CoinForce2 : MonoBehaviour {
	
	public float force;
	public Rigidbody rb2;
	public AudioClip impact;
	public AudioSource audio2;
	public float magnitude;
	private Vector3 otherVelocity;



	void Start ()
	{
		rb2 = GetComponent<Rigidbody> ();
		audio2 = GetComponent<AudioSource>();


	}
	

	void OnTriggerEnter (Collider other)
	{
		if (other.tag == "Player")
		{
			Debug.Log ("Working");

			rb2.AddForce(rb2.velocity * force);
			GetComponent<AudioSource>().PlayOneShot(impact, 0.7F);
		}
	}
	void OnTriggerExit (Collider other)
	{
		if (other.tag == "Player") 
		{
			otherVelocity = rb2.velocity.normalized;
			rb2.AddForce(otherVelocity * magnitude * Time.deltaTime, ForceMode.Impulse);

		}
	}



}