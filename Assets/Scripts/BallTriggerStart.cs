﻿using UnityEngine;
using System.Collections;

public class BallTriggerStart : MonoBehaviour {

	public float propel;
	public Rigidbody ball;
	public bool isEmpty;


	void Start()
	{
		gameObject.SetActive (true);
		isEmpty = false;
	}

	// Use this for initialization
	//void Update () 
	//{
		//ball = GetComponent<Rigidbody> ();
	//	isEmpty = false;
	//	gameObject.SetActive (false);
	//}
	
	// Update is called once per frame
	void OnTriggerStay (Collider other){
		if (other.tag == "Player")
		{
			isEmpty = true;
			gameObject.SetActive (false);
			Debug.Log ("propel");
			ball.AddForce(transform.forward * propel);


		}	
	}
}																																															
