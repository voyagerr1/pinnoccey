﻿using UnityEngine;
using System.Collections;

public class coinColor01 : MonoBehaviour {

	private Renderer rend;
	public Color onHit;
	public Color normal;
	private float duration = 10;

	// Use this for initialization
	void Start () {

		rend = GetComponent<Renderer> ();
	
	}
	
	void OnTriggerEnter (Collider other)
	{
		if (other.tag == "Player") {
			Debug.Log ("Working");
			
			rend.material.color = onHit;
		}

	}
		void OnTriggerExit (Collider other)
		{
			if (other.tag == "Player") 
			{
				
				rend.material.color = Color.Lerp (onHit,normal,duration);
				
			}
		}
}
