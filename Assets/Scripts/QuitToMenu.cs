﻿using UnityEngine;
using System.Collections;

public class QuitToMenu : MonoBehaviour {

	public void QuitMenu()
	{
		//If we are running in a standalone build of the game
		#if UNITY_STANDALONE
		//Quit the application
		Application.LoadLevel("Scene01-test08-GM");
		#endif
		
		//If we are running in the editor
		#if UNITY_EDITOR
		//Stop playing the scene
		UnityEditor.EditorApplication.isPlaying = false;
		#endif
	}
}