﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;
using System.Collections;


public class BallFaster : MonoBehaviour {



	public Text countText;
	public Text countText2;
	public Text winText;
	public Vector3 thrust;
	public Rigidbody rb;
	public SphereCollider bSphere;
	public BoxCollider g1;
	public BoxCollider g2;
	public Vector3 velocityHit;
	public float speed;
	public float slowDrag;
	public float slowDragUp;
	public float points;
	public float countA;
	public float countB;
	public bool lastTouchA = false;
	public bool lastTouchB = false;
	public bool isOver = false;
	private int count;
	private int count2;
	public AudioClip onHitPlay;
	public AudioSource ballAudio;
	public AudioClip winAudio;
	public AudioClip themeMusic;
	public AudioClip scorePoints;
	public bool musicOn = true;
	public Renderer rd;





	public Transform sucker;
	public float suckSpeed;
	public TrailRenderer tr;

	public string playerAscore;
	public string playerBscore;

	public bool canUseA1 = false;
	public bool canUseB1 = false;

	public Color pA;
	public Color pB;

	public float timerA = 5;

	public float timerB = 5;

	public float curTimerA = 5;
	public float curTimerB = 5;
	// public Text timeTextA;
	// public Text timeTextB;
	public string powerTimeA;
	public string powerTimeB;


	public GameObject coinA;
	public GameObject coinB;
	public float timerBarLengthA;

	public float timerBarLengthB;

	public SpriteRenderer sr;

	public Transform ballTrans;

	public bool isSmall = false;
	public bool isBig = false;

	public Image maskRight;
	public Image maskLeft;
	public float maskRightLength;
	public float maskLeftLength;
	public RectTransform maskTright;
	public RectTransform maskTleft;

	public Transform blackHole;
	public float blackHoleSpeed;

	public Rigidbody AIRight;
	public Rigidbody AILeft;
	public float speedF;
	public float speedR;


	void Start() {
		rb = GetComponent<Rigidbody>();
		ballAudio = GetComponent<AudioSource>();
		ballAudio.clip = themeMusic;
		ballAudio.Play ();
		rd = GetComponent<Renderer> ();
		tr = GetComponent<TrailRenderer> ();
		ballTrans = GetComponent<Transform> ();



		timerBarLengthA = Screen.width / 4;
		timerBarLengthB = Screen.width / 4;
		//gameAudio.Play ();
		maskRightLength = 0;
		maskLeftLength = 0;
		count = 0;
		SetCountTextA ();
		SetCountTextB ();
		winText.text = "";

	}


	void Update ()
	{
		//	AdjustCurrentTimeA (5);
		//	AdjustCurrentTimeB (5);
		maskLeftLength = countA * 2;
		maskRightLength = countB * 2;
		maskTright.sizeDelta = new Vector2 (maskRightLength, 50);
		maskTleft.sizeDelta = new Vector2 (maskLeftLength, 50);
		if (isSmall == true) {
			

			ballTrans.localScale = new Vector3 (0.6f, 0.6f, 0.6f);
		} else if (isBig == true) {
			//	OnHitBig(5);
		
			ballTrans.localScale = new Vector3 (2.5f, 2.5f, 2.5f);
		} else {
			ballTrans.localScale = new Vector3 (1.2f, 1.2f, 1.2f);
		}

	
	
		if (countA >= 120) {
			canUseA1 = true;
		}

		if (countB >= 120) {
			canUseB1 = true;
		}


	}


		void FixedUpdate ()
		{
			rb.velocity = rb.velocity.normalized * speed;
	//		timeTextA.text = timer.ToString(powerTime);

	

		if (Input.GetKey (KeyCode.LeftShift)&& countA >= 120 && canUseA1 == true && lastTouchA == true && timerA > 0) {


			AdjustCurrentTimeA (5);
			timerA -= Time.deltaTime;

			curTimerA = timerA;
			rb.drag = slowDrag;
			waitSomeA();
		//	timeTextA.text = timerA.ToString(powerTimeA);

		}

		else if (Input.GetKey (KeyCode.Keypad0)&& countB >= 120 && canUseA1 == true &&  lastTouchB == true && timerB > 0)
			{
			AdjustCurrentTimeB (5);
			timerB -= Time.deltaTime;
			curTimerB = timerB;
				rb.drag = slowDrag;
			waitSomeB();
		//	timeTextB.text = timerB.ToString(powerTimeB);
			}

		 else 
		{
			rb.drag = slowDragUp;
		}



	}

	 void OnCollisionEnter ()
	{

		GetComponent<AudioSource>().PlayOneShot(onHitPlay, 0.7F);
	}

	

	void OnTriggerExit (Collider other)
	{

		rb.AddForce(transform.rotation * velocityHit);


	}


		IEnumerator OnTriggerStay (Collider other)
	{
		if (other.tag == "inGoal") {
			rb.mass = 10000;
			float step = speed * Time.deltaTime;
			transform.position = Vector3.MoveTowards (transform.position, sucker.position, step);
			
			Debug.Log ("goal baby");
		}
		

		if (other.name == "Winning" && countA > countB) {

			ballAudio.Stop ();
			ballAudio.clip = winAudio;
			ballAudio.Play ();
			Debug.Log ("End");
			//bSphere.enabled = false;
			winText.color = pA;
			winText.text = "Blue Wins!!!";

			GetComponent<AudioSource> ().PlayOneShot (winAudio, 0.7F);
			isOver = true;
			yield return new WaitForSeconds (6);
			gameOver ();


	
			//Application.LoadLevel("Scene01-test08-GM");
		} else if (other.name == "Winning" && countA < countB) {
		
			ballAudio.Stop ();
			ballAudio.clip = winAudio;
			ballAudio.Play ();
			Debug.Log ("End");
			//bSphere.enabled = false;
			winText.color = pB;
			winText.text = "Red Wins!!!";
		
		
			GetComponent<AudioSource> ().PlayOneShot (winAudio, 0.7F);
			isOver = true;
			yield return new WaitForSeconds (6);
			gameOver ();


	
			//Application.LoadLevel("Scene01-test08-GM");
		}

		if (other.gameObject.tag == "Score" && lastTouchA == false) {
			count = count + 100;
			SetCountTextB ();

		} else if (other.gameObject.tag == "Score" && lastTouchB == false) {
			count2 = count2 + 100;
			SetCountTextA ();




		}

		if (other.tag == "blackHoleArea") {
		//	rb.velocity = new Vector3(0,0,0);
			ballTrans.position = Vector3.MoveTowards(transform.position, blackHole.position, blackHoleSpeed);
		}

		if (other.tag == "blackHole") {
			ballTrans.position = new Vector3 (0,1.02f,0);
			rb.velocity = rb.velocity.normalized * speed;
		}


	}


	void OnTriggerEnter(Collider other) 
	{

		if (other.name == "Winning" && countA > countB) {


			Debug.Log ("music stops");


		} else if (other.name == "Winning" && countA < countB) {


			Debug.Log ("music stops");

		}





		if (other.gameObject.tag == "PlayerA") {

			Debug.Log ("Player A");

			lastTouchA = true;
			rd.material.color = pA;
			tr.material.SetColor ("_TintColor", pA);
			sr.color = pA;
			lastTouchB = false;
			
		}
		if (other.gameObject.tag == "PlayerB") {

			lastTouchA = false;
			rd.material.color = pB;
			tr.material.SetColor ("_TintColor", pB);
			sr.color = pB;
			lastTouchB = true;
			Debug.Log ("Player b");

		} else if (other.tag == "Score") {
			count = count + 100;
			SetCountTextB ();		
		} else if (other.tag == "ScoreB") {
			count2 = count2 + 100;
			SetCountTextA ();

		
		}
		/*
		if (other.gameObject.tag == "powerUpA" && lastTouchA == true) {
			Destroy(coinA);
			canUseA = true;
			canUseB = false;
		} else if (other.gameObject.tag == "powerUpB" && lastTouchB == true){
			Destroy(coinB);
			canUseA = false;
			canUseB = true;
		}/*/

		if (other.gameObject.tag == "CoinsA" && lastTouchA == false) {
			count = count + 10;
			GetComponent<AudioSource>().PlayOneShot(scorePoints, 0.7F);
			SetCountTextB ();
		} 
		
		else if (other.gameObject.tag == "CoinsB" && lastTouchB == false) {
			count2 = count2 + 10;
			GetComponent<AudioSource>().PlayOneShot(scorePoints, 0.7F);
			SetCountTextA ();
			
			
		}

		if (other.gameObject.tag == "powerUpSmall") {

			StartCoroutine(OnHitSmall());
		} else if (other.gameObject.tag == "powerUpBig") {

			StartCoroutine(OnHitBig());
		} 


		if (other.gameObject.tag == "AIcolLeft") {
			AILeft.GetComponent<Rigidbody>().AddForce (transform.forward * speedF);
		} else {
			AILeft.GetComponent<Rigidbody>().AddForce (-transform.forward * speedR);
		}


		if (other.gameObject.tag == "AIcolRight") {
			AIRight.GetComponent<Rigidbody>().AddForce (transform.forward * speedF);
	} else {
			AIRight.GetComponent<Rigidbody>().AddForce (-transform.forward * speedR);
	}
		}






	void SetCountTextA ()
	{

		{
			countText.text = "" + count2.ToString (playerAscore);
		//	GetComponent<AudioSource>().PlayOneShot(scorePoints, 0.7F);
			countA = count2;


		}
	}

	void SetCountTextB ()
	{

		{
			countText2.text = "" + count.ToString (playerBscore);
			GetComponent<AudioSource>().PlayOneShot(scorePoints, 0.7F);
			countB = count;
		}
	}

	void gameOver ()
	{
		isOver = true;
		Application.LoadLevel("Rematch02");

	}

	void Play ()
	{
		musicOn = true;


	}

	void OnGUI (){   // settting health bar to change as it deacreases


			

	//	GUI.Box (new Rect (380, 50, timerBarLengthA, 20),"");
	//	GUI.Box (new Rect (1000, 50, timerBarLengthB, 20), "");

	

	}
	public void AdjustCurrentTimeA(int adjA){


		curTimerA += adjA;
		
		if (curTimerA < 0)
			curTimerA = 0;
		
		if (curTimerA > timerA)
			curTimerA = timerA;
		
		if (timerA < 5)
			timerA = curTimerA;
		
		timerBarLengthA = (Screen.width / 14) * (curTimerA);
	}
	public void AdjustCurrentTimeB(int adjB){

		curTimerB += adjB;
		
		if (curTimerB < 0)
			curTimerB = 0;
		
		if (curTimerB > timerB)
			curTimerB = timerB;
		
		if (timerB < 5)
			timerB = curTimerB;
		
		timerBarLengthB = (Screen.width / 14) * (curTimerB);
	}

	IEnumerator OnHitSmall(){

		isSmall = true;
		yield return new WaitForSeconds (4);
		isSmall = false;



	}
	 IEnumerator OnHitBig(){
		
		isBig = true;
		yield return new WaitForSeconds (4);
		isBig = false;
			


		
	}
	IEnumerator waitSomeA(){
		
		canUseA1 = false;
		yield return new WaitForSeconds (20);
		Debug.Log ("Blue Can use Slow Again");
		canUseA1 = true;

	}
	IEnumerator waitSomeB(){
		
		canUseB1 = false;
		yield return new WaitForSeconds (20);
		Debug.Log ("Red Can use Slow Again");
		canUseB1 = true;

	}



}