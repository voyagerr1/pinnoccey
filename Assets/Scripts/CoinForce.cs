﻿using UnityEngine;
using System.Collections;

public class CoinForce : MonoBehaviour {

	public float force;
	public Rigidbody rb2;
	public AudioClip impact;
	public AudioSource audio1;

	void Start ()
	{
		rb2 = GetComponent<Rigidbody> ();
		audio1 = GetComponent<AudioSource>();
	}


	void OnTriggerEnter (Collider other)
	{
		if (other.tag == "Player")
		{
			Debug.Log ("Working");
			rb2.AddForce(transform.forward * force);
			GetComponent<AudioSource>().PlayOneShot(impact, 0.7F);
			}
}
}