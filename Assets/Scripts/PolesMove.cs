﻿using UnityEngine;
using UnityEngine.Networking;
using System.Collections;

public class PolesMove : MonoBehaviour 
	
{

	public float poleSpeed;
	public GameObject poles;
	public Rigidbody polesA;
	public Transform polesAt;
	public AudioSource ad;
	public AudioClip up;
	public AudioClip down;





	//public Rigidbody polesB;

	
		

	void Update()
	{
		if (Input.GetKey(KeyCode.W)) {
			polesA.AddForce(Vector3.forward * poleSpeed * Time.deltaTime);
			playPolesUp();

		
			
		}
		else if (Input.GetKey(KeyCode.S)) 
		{
			polesA.AddForce (Vector3.back * poleSpeed * Time.deltaTime);
			playPolesDown();
		}
	}

	void playPolesUp()
	{
		if (Input.GetKeyDown(KeyCode.W)) {
			ad.PlayOneShot (up , 1f);
			
			
		}
	}
	
	void playPolesDown()		
	{
		if (Input.GetKeyDown(KeyCode.S)) {
			ad.PlayOneShot(down,1f);
			
		}
	}


}

