﻿using UnityEngine;
using System.Collections;

public class PaddleMove4 : MonoBehaviour {
	
	public static HingeJoint hinge;// = GetComponent<HingeJoint>();
	public static JointMotor motor;// = hinge.motor;
	public static JointLimits limits;
	
	public float targetVel = 400;
	public float power = 3000;
	public Rigidbody ballPaddle;
	private Vector3 otherVelocity;
	public float forceOut;
	public AudioSource swingSource;
	public AudioClip swingSound;
	// Use this for initialization
	void Start () {
		hinge = GetComponent<HingeJoint>();
		limits = hinge.limits;
		limits.min = 0;
		limits.bounciness = 0;
		limits.bounceMinVelocity = 0;
		limits.max = 90;
		hinge.limits = limits;
		hinge.useLimits = true;
		motor = hinge.motor;
		motor.force = 0;
		motor.targetVelocity = 90;
		motor.freeSpin = false;
		hinge.motor = motor;
		hinge.useMotor = true;
	}

	void Update(){
	
			InputMovement ();

	}
	

	
	// Update is called once per frame
	void InputMovement () {
		if (Input.GetKey (KeyCode.RightArrow)) {
			//Debug.Log ("Space Was Pressed");

			limits.min = 0;
			limits.bounciness = 0;
			limits.bounceMinVelocity = 0;
			limits.max = 90;
			hinge.limits = limits;
			hinge.useLimits = false;
			motor.force = power;
			motor.targetVelocity = targetVel;
			motor.freeSpin = false;
			hinge.motor = motor;
			hinge.useMotor = true;
		} else {
			motor.force = 0;
			motor.targetVelocity = targetVel;
			motor.freeSpin = false;
			hinge.motor = motor;
			hinge.useMotor = true;
			hinge.useLimits = true;
		}
		if (Input.GetKeyDown (KeyCode.RightArrow)) {
			GetComponent<AudioSource>().PlayOneShot(swingSound, 0.7F);
		}
	
	}
	void OnCollisionExit (Collision other)
		
	{
		if (gameObject.CompareTag ("Player")) {
			otherVelocity = ballPaddle.velocity.normalized;
			ballPaddle.AddForce (otherVelocity * forceOut);
		}
	}

	
}
