﻿using UnityEngine;
using UnityEngine.Networking;
using System.Collections;

public class PolesMove2 : MonoBehaviour 
	
{

	public float poleSpeed;
	public GameObject poles;
	public Rigidbody polesA;
	public Transform polesAt;
	public AudioSource ad;
	public AudioClip up;
	public AudioClip down;
	public GameObject theBall;
	public float speed = 60;
	public float lerpSpeed = 1f;
	private Rigidbody rigidBody;
	
	
	
	
	//public Rigidbody polesB;

	void Start (){
		rigidBody = GetComponent<Rigidbody> ();

	}

	
	
	
	void Update()
	{




		if (Input.GetKey(KeyCode.UpArrow)) {
			polesA.AddForce(Vector3.forward * poleSpeed * Time.deltaTime);
			playPolesUp();
			
			
			
		}
		else if (Input.GetKey(KeyCode.DownArrow)) 
		{
			polesA.AddForce (Vector3.back * poleSpeed * Time.deltaTime);
			playPolesDown();
		}
	}
	
	void playPolesUp()
	{
		if (Input.GetKeyDown(KeyCode.UpArrow)) {
			ad.PlayOneShot (up , 1f);
			
			
		}
	}
	
	void playPolesDown()
	{
		if (Input.GetKeyDown(KeyCode.DownArrow)) {
			ad.PlayOneShot(down,1f);
			
		}
	}
	


	void FixedUpdate () {
		
		if (theBall.transform.position.z > transform.position.z)
		{
			if (rigidBody.velocity.z < 0) rigidBody.velocity = new Vector3 (0, 0, 0);
			rigidBody.velocity = Vector3.Lerp(rigidBody.velocity,new Vector3(0,0,1) * speed, lerpSpeed * Time.deltaTime);
		}
		else if (theBall.transform.position.z < transform.position.z)
		{
			if (rigidBody.velocity.z > 0) rigidBody.velocity = new Vector3 (0, 0, 0);
			rigidBody.velocity = Vector3.Lerp(rigidBody.velocity, new Vector3(0,0,-1) * speed, lerpSpeed * Time.deltaTime);
		}
		else
		{
			rigidBody.velocity = Vector3.Lerp(rigidBody.velocity, new Vector3(0,0,0) * speed, lerpSpeed * Time.deltaTime);
		}

	}

}

