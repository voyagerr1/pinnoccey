﻿using UnityEngine;
using UnityEngine.Networking;
using System.Collections;


public class TestNetManager : NetworkManager {

public override void OnServerAddPlayer(NetworkConnection conn, short playerControllerId)
{
	GameObject proxyPlayer = (GameObject)Instantiate(playerPrefab,playerPrefab.transform.position,playerPrefab.transform.rotation);
	NetworkServer.AddPlayerForConnection(conn, proxyPlayer, playerControllerId);
}
}


class fixNet : NetworkBehaviour {


	[SyncVar]
	Vector3 rotation;

	public override void OnStartServer()      
	{
		rotation = transform.rotation.eulerAngles;
	}
	public override void OnStartClient()
	{
		transform.rotation = Quaternion.Euler(rotation);
	}
}



