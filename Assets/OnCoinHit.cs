﻿using UnityEngine;
using System.Collections;

public class OnCoinHit : MonoBehaviour {


	public AudioClip onHit;




	void OnTriggerEnter(Collider other){
		GetComponent<AudioSource>().PlayOneShot(onHit, 0.7F);
	}
}
