﻿using UnityEngine;
using System.Collections;

public class paddleTesting : MonoBehaviour {

	[Tooltip("Force Used When Moving Flipper")]
	public	float		flipForce		= 500f;
	
	[Tooltip("Caching Rigidbody")]
	public	Rigidbody	rb;





	void Awake ()
	{
		rb = GetComponent<Rigidbody>();
	}

	void FixedUpdate ()
	{
		if (Input.GetKey (KeyCode.A)) {
			rb.AddForce ( new Vector3 ( 0f, flipForce, 0f) );
		} else {
			rb.AddForce ( new Vector3 ( 0f, -flipForce, 0f) );
		}
	
	}
	









	public void FlipUp ()
	{
		// ADD FORCE TO PULL LEFT FLIPPER UP
		rb.AddForce ( new Vector3 ( 0f, flipForce, 0f) );
		
		// PLAY SOUND ONCE AND WAIT UNTIL FLIPPER IS PULLED DOWN
	//	if ( this.canPlaySound == true )
	//	{
			// PLAY SOUND
		//	MusicManager.Instance.PlayFlipperUp ();
			
		//	this.canPlaySound = false;
		//}
	}
	
	public void FlipDown ()
	{
		// ADD FORCE TO PULL LEFT FLIPPER DOWN
		rb.AddForce ( new Vector3 ( 0f, -flipForce, 0f) );
		
	//	this.canPlaySound = true;
	}




}

