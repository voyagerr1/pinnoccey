using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class BallGenerator2d : MonoBehaviour
{
	public	static BallGenerator2d	Instance;

	[Tooltip("List Of Balls That Can Spawn In The Pinball")]
	public	List<Ball2d>			ballFreeList;

	[Tooltip("List Of Balls That Are Inside The Pinball")]
	public	List<Ball2d>			ballInGameList;

	[Tooltip("List Of Points Where Balls Can Spawn")]
	public	Transform[]				spawnPoints;
	
	[Tooltip("Prefab Of The Ball Element")]
	public	GameObject				ballPrefab;
	
	#if UNITY_EDITOR
	void OnDrawGizmos ()
	{
		// USE WIRE SPHERE TO SHOW SPAWN POINT IN EDITOR
		Gizmos.color = Color.yellow;
		for ( int i = 0; i < this.spawnPoints.Length; i++ )
		{
			Gizmos.DrawWireSphere ( this.spawnPoints [ i ].position, 0.25f );
		}
	}
	#endif

	void Awake ()
	{
		Instance	= this;

		// CREATE A POOL OF 5 BALLS
		for ( int i = 0; i < 5; i++ )
		{
			// INSTANTIATE A NEW BALL FROM PREFAB
			GameObject __go			= ( GameObject ) Instantiate ( this.ballPrefab );
			Ball2d __ball 			= __go.GetComponent<Ball2d>();
			__ball.tr.parent		= this.transform;
			// PLACE IT OUTSIDE THE CAMERA
			__ball.tr.position		= new Vector3 ( -2000f, -2000f, 0f );
			// DISABLE PHYSICS
			__ball.rb.isKinematic	= true;
			this.ballFreeList.Add ( __ball );
		}
	}

	// SPAWN A NEW BALL
	public void SpawnBall ()
	{
		Ball2d __ball;

		// CHECK IF THERE IS A BALL IN ballFreeList THAT CAN BE USED
		if ( this.ballFreeList.Count > 0 )
		{
			__ball					= this.ballFreeList [ 0 ];
			this.ballFreeList.Remove ( __ball );
		}
		// NO FREE BALLS IN ballFreeList
		else
		{
			// INSTANTIATE A NEW BALL FROM PREFAB
			GameObject __go			= ( GameObject ) Instantiate ( this.ballPrefab );
			__ball 					= __go.GetComponent<Ball2d>();
			__ball.tr.parent 			= this.transform;
			__ball.tr.position		= new Vector3 ( -2000f, -2000f, 0f );
		}

		this.ballInGameList.Add ( __ball );

		// POSITION THE BALL IN A RANDOM POSITION
		__ball.tr.position			= this.spawnPoints [ Random.Range ( 0, this.spawnPoints.Length ) ].position;

		// ENABLE PHYSICS
		__ball.rb.isKinematic		= false;

		__ball.isAlive				= true;

	}

	// PUT _ball IN ballFreeList FOR REUSE LATER
	// CALLED WHEN A BALL HIT BOX TRIGGER RECYCLEBALLZONE
	public void RecycleBall ( Ball2d __ball )
	{
		// CHECK IF _ball WAS IN GAME
		if ( this.ballInGameList.Contains ( __ball ) )
		{
			__ball.isAlive 			= false;
			// DISABLE PHYSICS
			__ball.rb.isKinematic	= true;
			// PLACE IT OUTSIDE THE CAMERA
			__ball.tr.position		= new Vector3 ( -2000f, -2000f, 0f );

			// REMOVE FROM ballInGameList
			this.ballInGameList.Remove ( __ball );
			// ADD TO ballFreeList
			this.ballFreeList.Add ( __ball );
		}
	}

	// CALLED BY GameLogic2d.cs WHEN GAME STARTS
	public void OnGameStart ()
	{
		// SPAWN A NEW BALL WHEN GAME STARTS
		this.SpawnBall ();
	}
}
