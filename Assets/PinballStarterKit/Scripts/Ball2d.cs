﻿using UnityEngine;
using System.Collections;

public class Ball2d : MonoBehaviour
{
	[Tooltip("Define If This Ball Is Inside The Game")]
	public	bool			isAlive;

	[Tooltip("Caching Transform")]
	public	Transform		tr;

	[Tooltip("Caching Rigidbody")]
	public	Rigidbody2D		rb;

	[Tooltip("Ball Sprite GameObject")]
	public	GameObject		ballSprite;
	
	void Awake ()
	{
		this.tr 			= this.GetComponent<Transform>();
		this.rb 			= this.GetComponent<Rigidbody2D>();
		this.rb.isKinematic = true;
	}

	void OnTriggerEnter2D ( Collider2D __c )
	{
		if ( this.isAlive == true )
		{
			if ( __c.CompareTag ( "DEADZONE" ) )
			{
				// GAME IS OVER IF HIT TRIGGER DEADZONE
				GameLogic2d.Instance.GameOver ();
			}
			else if ( __c.CompareTag ( "RECYCLEBALLZONE" ) )
			{
				// RECYCLE THIS BALL
				BallGenerator2d.Instance.RecycleBall ( this );
			}
		}
	}
}
