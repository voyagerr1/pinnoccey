﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class StarGenerator2d : MonoBehaviour
{
	public	static StarGenerator2d	Instance;

	[Tooltip("List Of Stars That Can Spawn In The Pinball")]
	public	List<Star2d>			starFreeList;

	[Tooltip("List Of Stars That Are Inside The Pinball")]
	public	List<Star2d>			starInGameList;
	
	[Tooltip("List Of Points Where Stars Can Spawn")]
	public	Transform[]				spawnPoints;
	
	[Tooltip("Prefab Of The Star Element")]
	public	GameObject				starPrefab;

	#if UNITY_EDITOR
	void OnDrawGizmos ()
	{
		// USE WIRE SPHERE TO SHOW SPAWN POINT IN EDITOR
		Gizmos.color = Color.cyan;
		for ( int i = 0; i < this.spawnPoints.Length; i++ )
		{
			Gizmos.DrawWireSphere ( this.spawnPoints [ i ].position, 0.25f );
		}
	}
	#endif

	void Awake ()
	{
		Instance	= this;

		// CREATE A POOL OF 5 STARS
		for ( int i = 0; i < 5; i++ )
		{
			// INSTANTIATE A NEW STAR FROM PREFAB
			GameObject __go			= ( GameObject ) Instantiate ( this.starPrefab );
			Star2d __star 			= __go.GetComponent<Star2d>();
			__star.tr.parent		= this.transform;
			// PLACE IT OUTSIDE THE CAMERA
			__star.tr.position		= new Vector3 ( -3000f, -3000f, 0f );
			this.starFreeList.Add ( __star );
		}
	}
	
	// SPAWN A NEW STAR
	public void SpawnStar ()
	{
		CancelInvoke ( "SpawnAfterAPeriod" );
		// WAIT ONE SECOND AND SPAWN A STAR
		Invoke ( "SpawnAfterAPeriod", 1.0f );
	}

	void SpawnAfterAPeriod ()
	{
		Star2d __star;
		
		// CHECK IF THERE IS A STAR IN starFreeList THAT CAN BE USED
		if ( this.starFreeList.Count > 0 )
		{
			__star					= this.starFreeList [ 0 ];
			this.starFreeList.Remove ( __star );
		}
		// NO FREE BALLS IN starFreeList
		else
		{
			// INSTANTIATE A NEW STAR FROM PREFAB
			GameObject __go			= ( GameObject ) Instantiate ( this.starPrefab );
			__star 					= __go.GetComponent<Star2d>();
			__star.tr.parent 			= this.transform;
			__star.tr.position		= new Vector3 ( 3000f, -3000f, 0f );
		}
		
		this.starInGameList.Add ( __star );
		
		// POSITION THE STAR IN A RANDOM POSITION
		__star.tr.position			= this.spawnPoints [ Random.Range ( 0, this.spawnPoints.Length ) ].position;
		
		__star.isCollected			= false;

		__star.Spawn ();
	}
	
	public void RecycleStar ( Star2d __star )
	{
		// CHECK IF _ball WAS IN GAME
		if ( this.starInGameList.Contains ( __star ) )
		{
			// PLACE IT OUTSIDE THE CAMERA
			__star.tr.position		= new Vector3 ( -3000f, -3000f, 0f );

			__star.Reset ();
			
			// REMOVE FROM starInGameList
			this.starInGameList.Remove ( __star );
			// ADD TO starFreeList
			this.starFreeList.Add ( __star );
		}
		
	}
	
	public void OnGameStart ()
	{
		// SPAWN A STAR WHEN GAME STARTS
		this.SpawnStar ();
	}

	public void OnGameOver ()
	{
		// HIDE ALL STARS IN GAME
		while ( this.starInGameList.Count > 0 )
		{
			this.RecycleStar ( this.starInGameList [ 0 ] );
		}
	}
}
