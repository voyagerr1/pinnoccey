﻿using UnityEngine;
using System.Collections;

public class MusicManager : MonoBehaviour
{
	public	static MusicManager		Instance;

	[Tooltip("Audio When Pressing Button")]
	public	AudioClip				audioClipMenuButton;
	[Tooltip("Audio Source Used When Pressing Button")]
	public	AudioSource				audioSourceMenu;

	[Tooltip("Audio When Flipper Flips Up")]
	public	AudioClip				audioClipGameFlipper;
	[Tooltip("Audio When Ball Hits Bumper")]
	public	AudioClip				audioClipGameBumper;
	[Tooltip("Audio When Ball Appears In Pinball")]
	public	AudioClip				audioClipGameBallSpawn;
	[Tooltip("Audio When Star Appears In Pinball")]
	public	AudioClip				audioClipGameStarSpawn;
	[Tooltip("Audio When Ball Hits A Star")]
	public	AudioClip				audioClipGameStarCollect;
	[Tooltip("Audio Source Of Flipper")]
	public	AudioSource				audioSourceFlipper;
	[Tooltip("Audio Source Of Ball Events")]
	public	AudioSource				audioSourceBall;

	void Awake ()
	{
		Instance = this;
	}
	
	public void PlayMenuButtonPressed ()
	{
		this.audioSourceMenu.clip = this.audioClipMenuButton;
		this.audioSourceMenu.loop = false;
		this.audioSourceMenu.Play ();
	}

	public void PlayFlipperUp ()
	{
		this.audioSourceFlipper.clip = this.audioClipGameFlipper;
		this.audioSourceFlipper.loop = false;
		this.audioSourceFlipper.Play ();
	}
	
	public void PlayBumper ()
	{
		this.audioSourceBall.clip = this.audioClipGameBumper;
		this.audioSourceBall.loop = false;
		this.audioSourceBall.Play ();
	}

	public void PlayStarCollect ()
	{
		this.audioSourceBall.clip = this.audioClipGameStarCollect;
		this.audioSourceBall.loop = false;
		this.audioSourceBall.Play ();
	}
}
