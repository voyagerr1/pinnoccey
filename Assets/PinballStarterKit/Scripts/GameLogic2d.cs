﻿using UnityEngine;
using System.Collections;

public class GameLogic2d : MonoBehaviour
{
	public	static GameLogic2d		Instance;

	public	enum STATE
	{
		Menu,
		Game,
		GameOver
	}
	public	STATE					state;

	[Tooltip("Left Flipper Controller")]
	public	Flipper2d				flipperLeft;
	[Tooltip("Right Flipper Controller")]
	public	Flipper2d				flipperRight;

	[Tooltip("Defines If Left Screen Pressed")]
	public	bool					isLeftScreenPressed;
	[Tooltip("Defines If Right Screen Pressed")]
	public	bool					isRightScreenPressed;

	[Tooltip("Caching Size Of Half Screen")]
	private	float					screenHalfWidth;

	[Tooltip("This Is The Score When Game Is Playing")]
	private	int						currentScore;

	void Awake ()
	{
		Instance					= this;

		Application.targetFrameRate = 60;

		this.screenHalfWidth		= Screen.width * 0.5f;
	}

	void Start ()
	{
		this.state = STATE.Menu;

		// SHOW START MENU
		MenuLogic2d.Instance.MenuShow ();
	}

	void FixedUpdate ()
	{
		this.HandleFlippers ();
	}

	void Update ()
	{
		this.HandlePlayerInput ();
	}

	#region FLIPPERS CONTROL

	void HandleFlippers ()
	{
		// CONTROL LEFT FLIPPER UP AND DOWN
		if ( this.isLeftScreenPressed == true )
		{
			this.flipperLeft.FlipUp ();
		}
		else
		{
			this.flipperLeft.FlipDown ();
		}
		
		// CONTROL RIGHT FLIPPER UP AND DOWN
		if ( this.isRightScreenPressed == true )
		{
			this.flipperRight.FlipUp ();
		}
		else
		{
			this.flipperRight.FlipDown ();
		}
	}

	#endregion

	#region PLAYER INPUT

	void HandlePlayerInput ()
	{
		// CHECK IF PLAYER IS PRESSING LEFT SIDE OR RIGHT SIDE OF THE SCREEN
		
		// EDITOR DEBUGGING
		#if UNITY_EDITOR
		if ( Input.GetKeyDown ( KeyCode.Space ) )
		{
			if ( this.state == STATE.Menu )
			{
				MenuLogic2d.Instance.PressMenuButtonPlay ();
			}
			else if ( this.state == STATE.GameOver )
			{
				MenuLogic2d.Instance.PressGameOverButtonRestart ();
			}
		}
		#endif
		
		// GAME IS RUNNING
		if ( this.state == STATE.Game )
		{
			this.isLeftScreenPressed 		= false;
			this.isRightScreenPressed 		= false;
			
			#if UNITY_EDITOR
			if ( Input.GetMouseButton ( 0 ) )
			{
				if ( Input.mousePosition.x < this.screenHalfWidth )
				{
					this.isLeftScreenPressed 	= true;
				}
				if ( Input.mousePosition.x > this.screenHalfWidth )
				{
					this.isRightScreenPressed	= true;
				}
			}
			if ( Input.GetKey ( KeyCode.LeftShift ) )
			{
				this.isLeftScreenPressed 	= true;
			}
			if ( Input.GetKey ( KeyCode.RightShift ) )
			{
				this.isRightScreenPressed	= true;
			}
			if ( Input.GetKeyDown ( KeyCode.Space ) )
			{
				if ( this.state == STATE.Menu )
				{
					MenuLogic2d.Instance.PressMenuButtonPlay ();
				}
				else if ( this.state == STATE.GameOver )
				{
					MenuLogic2d.Instance.PressGameOverButtonRestart ();
				}
			}
			#endif
			
			// MOBILE INPUT
			int __touchCount = Input.touchCount;
			if ( __touchCount > 0 )
			{
				
				for ( int i = 0; i < __touchCount; i++ )
				{
					if (   Input.touches [ i ].phase == TouchPhase.Began
					    || Input.touches [ i ].phase == TouchPhase.Moved
					    || Input.touches [ i ].phase == TouchPhase.Stationary )
					{
						Vector3 __touchPos = Input.touches [ i ].position;
						if ( __touchPos.x < this.screenHalfWidth )
						{
							this.isLeftScreenPressed 	= true;
						}
						if ( __touchPos.x > this.screenHalfWidth )
						{
							this.isRightScreenPressed	= true;
						}
					}
				}
			}
		}	}
	#endregion

	#region GAME LOGIC

	public void GameStart ()
	{
		this.currentScore = 0;

		MenuLogic2d.Instance.GameShow ();

		BallGenerator2d.Instance.OnGameStart ();

		StarGenerator2d.Instance.OnGameStart ();

		this.state = STATE.Game;
	}

	public void GameRestart ()
	{
		this.GameStart ();
	}

	#endregion

	public void GameOver ()
	{
		int __bestScore = this.GetBestScore ();
		if ( this.currentScore > __bestScore )
		{
			this.SaveBestScore ( this.currentScore );
		}


		StarGenerator2d.Instance.OnGameOver ();
		
		MenuLogic2d.Instance.GameOverShow ();

		this.state = STATE.GameOver;
	}

	#region SCORE

	public int GetCurrentScore ()
	{
		return this.currentScore;
	}
	
	public void AddScore ( int __score )
	{
		this.currentScore += __score;
		
		// UPDATE SCORE IN HUD
		MenuLogic2d.Instance.GameUpdate (); 
		
		// SPAWN A NEW STAR
		StarGenerator2d.Instance.SpawnStar ();
	}

	public int GetBestScore ()
	{
		return PlayerPrefs.GetInt ( "BESTSCORE", 0 );
	}
	
	public void SaveBestScore ( int __score )
	{
		PlayerPrefs.SetInt ( "BESTSCORE", __score );
	}

	#endregion
}
