﻿using UnityEngine;
using System.Collections;

public class Star2d : MonoBehaviour
{
	public	bool		isCollected;

	[Tooltip("Caching Transform")]
	public	Transform 	tr;

	[Tooltip("Sprite GameObject Which Contains Animations")]
	public	GameObject	starSprite;

	void Awake ()
	{
		this.tr = this.GetComponent<Transform>();
	}
	
	void OnTriggerEnter2D ( Collider2D __c )
	{
		if ( this.isCollected == false )
		{
			if ( __c.CompareTag ( "BALL" ) )
			{
				MusicManager.Instance.PlayStarCollect ();

				this.isCollected = true;

				// PLAY ANIMATION WHEN BALL HIT STAR
				this.starSprite.GetComponent<Animation>().Play ( "StarHit" );

				// ADD ONE POINT TO THE CURRENT SCORE
				GameLogic2d.Instance.AddScore ( 1 );

				// MAKE THE CAMERA SHAKE
				GameCamera.Instance.Shake ( 1f, 1f, 0.5f );

				// RECYCLE STAR
				CancelInvoke ( "Recycle" );
				Invoke ( "Recycle", 0.5f );
			}
		}
	}

	public void Spawn ()
	{
		// PLAY ANIMATION WHEN STAR APPEARS
		this.starSprite.GetComponent<Animation>().Play ( "StarSpawn" );		
	}

	void Recycle ()
	{
		// RECYLCE STAR
		StarGenerator2d.Instance.RecycleStar ( this );
	}

	public void Reset ()
	{
		// RESET THE SCALE THAT HAS BEEN CHANGE WHILE PLAYING ANIMATIONS
		this.starSprite.transform.localScale = new Vector3 ( 1f, 1f, 1f );
	}
}
