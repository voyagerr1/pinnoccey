﻿using UnityEngine;
using System.Collections;

public class Flipper2d : MonoBehaviour
{
	[Tooltip("Boolean That Define If Can Play Flipper Sound")]
	public	bool		canPlaySound;
	
	[Tooltip("Force Used When Moving Flipper")]
	public	float		flipForce		= 500f;

	[Tooltip("Caching Rigidbody")]
	private	Rigidbody	rb;

	void Awake ()
	{
		this.rb = this.GetComponent<Rigidbody>();
	}

	public void FlipUp ()
	{
		// ADD FORCE TO PULL LEFT FLIPPER UP
		this.rb.AddForce ( new Vector3 ( 0f, this.flipForce, 0f) );

		// PLAY SOUND ONCE AND WAIT UNTIL FLIPPER IS PULLED DOWN
		if ( this.canPlaySound == true )
		{
			// PLAY SOUND
			MusicManager.Instance.PlayFlipperUp ();

			this.canPlaySound = false;
		}
	}

	public void FlipDown ()
	{
		// ADD FORCE TO PULL LEFT FLIPPER DOWN
		this.rb.AddForce ( new Vector3 ( 0f, -this.flipForce, 0f) );

		this.canPlaySound = true;
	}
}
