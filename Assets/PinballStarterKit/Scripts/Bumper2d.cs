﻿using UnityEngine;
using System.Collections;

public class Bumper2d : MonoBehaviour
{
	[Tooltip("Bumper Force Apply To The Colliding GameObject")]
	public 	float		bumperForce 	= 4f;

	[Tooltip("Sprite GameObject Which Contains The Animation BumperHit")]
	public	GameObject	bumperSprite;
	
	void OnCollisionEnter2D ( Collision2D __c )
	{
		// PLAY BUMP ANIMATION
		this.bumperSprite.GetComponent<Animation>().Play ( "BumperHit" );

		// PLAY SOUND
		MusicManager.Instance.PlayBumper ();

		// ADD INVERSE FORCE TO GAMEOBJECT THAT COLLIDES
		foreach ( ContactPoint2D __contactPoint in __c.contacts )
		{
			__contactPoint.collider.GetComponent<Rigidbody2D>().AddForce ( -1f * __contactPoint.normal * this.bumperForce );
		}
	}
}
